#pragma once

#include "image.h"

#pragma pack(push, 1)
struct bmp_header 
{
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};
#pragma pack(pop)

enum bmp_status {
    BMP_OK = 0,
    BMP_INVALID_SIGNATURE,
    BMP_INVALID_BITS,
    BMP_INVALID_HEADER,
    BMP_INVALID_MEMORY_ALLOCATION,
    BMP_INVALID_BIT_COUNT,
    BMP_INVALID_PADDING
};

enum bmp_status from_bmp(FILE* in, struct image* img);

enum bmp_status to_bmp(FILE* out, struct image const* img);
