#include "rotate.h"


struct image rotate(struct image const source) {
    struct image rotated = image_create(source.height, source.width);
    for (uint64_t h = 0; h < source.height; h++) {
        for (uint64_t w = 0; w < source.width; w++) {
            rotated.data[(source.width - w - 1) * rotated.width + h] = source.data[h * source.width + w];
        }
    }
    return rotated;
}
